const toxicModel = require('../models/toxic');
const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { generalRateLimiter } = require("../utils");

const bot = new Composer();

bot.hears([/^токс$/i, /^токсик$/i, /^токсично$/i, /^токсичненько$/i], async (ctx, next) => {
  console.log('токс ресивед', ctx.message);

  // skip for тач
  if (ctx.message.chat.id === -1001526858418){
    return next();
  }

  if (!ctx?.message?.reply_to_message?.from?.id){
    ctx.reply("А кто токсил-то? Реплайни на него.", Extra.inReplyTo(ctx.message.message_id));
    return next();
  }
  if (!ctx?.message?.reply_to_message?.from?.id === ctx?.message?.from?.id){
    ctx.reply("Ты дебил сам на себя токсить?", Extra.inReplyTo(ctx.message.message_id));
    return next();
  }

  const updatedToxic = await toxicModel.findOneAndUpdate({
    userId: ctx?.message?.reply_to_message?.from?.id , 
    chatId: ctx.message.chat.id
  }, {
    $inc: {toxicCounter: 1}, 
    first_name: ctx?.message?.reply_to_message?.from?.first_name
  }, {new: true, upsert: true});


  // console.log('toxic', updatedToxic);
  ctx.reply(`Уууу, жёсткий токс. Я добавил ${ctx?.message?.reply_to_message?.from?.first_name} очко токса. Счёт: ${updatedToxic.toxicCounter}`, Extra.inReplyTo(ctx.message.message_id));
  next();
});

bot.command('toxic', async (ctx, next) => {

  // skip for тач
  if (ctx.message.chat.id === -1001526858418){
    ctx.reply('Эта фича отключена в этом чате', {parse_mode: 'markdown', disable_notification: true});
    return next();
  }

  const toxicTable = await toxicModel.find({chatId: ctx.message.chat.id}).sort({toxicCounter: 'desc'}).limit(10).exec();

  // console.log('toxicTable', toxicTable)
  let text = `Топ-10 токсиков этого чата:\r\n`;
  for (const [index, toxicMan] of toxicTable.entries()){
    // console.log('index:', index);
    // console.log('toxicman', toxicMan);
    text = text + `${(index === 0 ? '🥇' : '')}${(index === 1 ? '🥈' : '')}${(index === 2 ? '🥉' : '')} [${toxicMan.first_name}](tg://user?id=${toxicMan.userId}) - ${toxicMan.toxicCounter} очков \r\n`;
  }

  // console.log('generated text', text);

  ctx.reply(text, {parse_mode: 'markdown', disable_notification: true})
})


module.exports = bot;
