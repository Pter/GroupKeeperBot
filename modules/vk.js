const axios = require("axios");
const FormData = require("form-data");
const tgUpload = require('../tgUpload');
const {Api} = require('telegram');

const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { nonDeletingRateLimiter, downloadBuffer } = require("../utils");
const config = require("../config");

const bot = new Composer();

const handleVkVideo = async(ctx, next) => {
  const { url } = ctx;

  console.log("received vk video:", url);

  const video = url.replace("https://vk.com/video", "");

  console.log("video field:", video);
  const bodyFormData = new FormData();

  bodyFormData.append("videos", video);

  const formHeaders = bodyFormData.getHeaders();

  const vkAnswer = await axios.post(
    `https://api.vk.com/method/video.get?access_token=${config.vkToken}&v=5.103`,
    bodyFormData,
    {
      headers: {
        ...formHeaders
      }
    }
  );

  console.log("vkanswer:", vkAnswer.data);

  if (
    vkAnswer.data &&
    vkAnswer.data.response &&
    Array.isArray(vkAnswer.data.response.items) &&
    vkAnswer.data.response.items.length > 0
  ) {
    const video = vkAnswer.data.response.items[0];

    if (video.files) {
      console.log("we have files:", video.files);
      let selectedFormat = Object.keys(video.files)[0];

      if (video.files.mp4_360) {
        selectedFormat = "mp4_360";
      }
      if (video.files.mp4_480) {
        selectedFormat = "mp4_480";
      }
      if (video.files.mp4_720) {
        selectedFormat = "mp4_720";
      }

      const videoUrl = video.files[selectedFormat];

      console.log('downloading vk buffer');

      const buffer = await downloadBuffer(videoUrl);

      console.log('vk video buffer:', buffer.length);

      // const uploadedFile = await tgUpload.uploadFile(buffer, ctx.message.chat.id);

      // console.log("uploadedFile", uploadedFile);


      buffer.name = 'vid.mp4';

      const thumb = video.image.find( x => x.width === Math.max(...video.image.filter(x => x.width < 400).map(x => x.width)));

      const thumbBuffer = await downloadBuffer(thumb.url);

      console.log('thumb buffer size:', thumbBuffer.length);

      const client = await tgUpload.getClient();

      const result = await client.sendMessage(ctx.message.chat.id, {
        file: buffer,
        message: `<b>${video.title}</b> \r\n ${video.description}`,
        replyTo: ctx.message.message_id,
        workers: 14,
        supportsStreaming: true,
        thumb: thumbBuffer,
        parseMode: 'html',
        attributes: [new Api.DocumentAttributeVideo({
          w: thumb.width,
          h: thumb.height,
          duration: video.duration,
          supportsStreaming: true
        })]
      })

      console.log('vk result', result);
      // ctx
      //   .replyWithVideo(videoUrl, Extra.inReplyTo(ctx.message.message_id))
      //   .catch(err => {
      //     console.log("video download error:", err);
      //     ctx.replyWithMarkdown(
      //       `[Ссылка](${videoUrl})`,
      //       Extra.inReplyTo(ctx.message.message_id)
      //     );
      //   });
    }
  }
};

bot.hears(
  (text, ctx) => {
    console.log("hello from vk handler");
    if (ctx.message.entities) {
      for (const entity of ctx.message.entities.filter(
        x => x.type === "url"
      )) {
        const url = ctx.message.text.substring(
          entity.offset,
          entity.length + entity.offset
        );

        console.log("urlqq vk is:", `"${url}"`);
        if (url.startsWith("https://vk.com/video")) {
          console.log("STARTWITH");
          ctx.url = url;
          return true;
        }
      }
    }
    return false;
  },
  nonDeletingRateLimiter,
  handleVkVideo
);

module.exports = bot;
