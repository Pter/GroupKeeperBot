const Composer = require("telegraf/composer");
// Async import got, similar to the Instagram module adjustments
const { Extra } = require("telegraf");
const { nonDeletingRateLimiter, downloadBuffer } = require("../utils");

const bot = new Composer();

// Simplified caption making function assuming no metadata from Cobalt
function makeCaption(url) {
  // Since no metadata is available, return a generic message or use a placeholder
  return `Check out this video from TikTok: ${url}`;
}

const handleTiktokPost = async (ctx, next) => {
  if (!ctx.url){
    console.log('No TikTok URL found in the message');
    return next();
  }

  const { got } = await import("got");
  const cobaltEndpoint = 'https://co.wuk.sh/api/json';
  try {
    const response = await got.post(cobaltEndpoint, {
      json: { url: ctx.url },
      responseType: 'json',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    if (['success', 'stream', 'redirect'].includes(response.body.status) && response.body.url) {
      // Directly try to send the video URL from Cobalt
      const caption = makeCaption(ctx.url);
      ctx.replyWithVideo(response.body.url, Extra.HTML().caption(caption).inReplyTo(ctx.message.message_id)).catch(async (err) => {
        console.log("Direct video URL failed, attempting download and upload:", err.message);
        // Attempt to download the video and upload if the direct URL fails
        const buffer = await downloadBuffer(response.body.url);
        if (buffer && buffer.byteLength > 0) {
          await ctx.replyWithVideo({ source: buffer }, Extra.HTML().caption(caption).inReplyTo(ctx.message.message_id));
        } else {
          console.log('Failed to download video, falling back to link');
          ctx.replyWithMarkdown(`[Link](${ctx.url})`, Extra.inReplyTo(ctx.message.message_id));
        }
      });
    } else {
      console.log('Cobalt API did not return a success status:', response.body);
      // Handle non-success status appropriately
    }
  } catch (error) {
    console.error('Error calling Cobalt API or handling the response:', error);
    // Handle errors appropriately
  }
};

bot.hears(
  async (text, ctx) => {
    if (ctx.message.entities) {
      const urls = ctx.message.entities
        .filter(entity => entity.type === 'url' || entity.type === 'text_link')
        .map(entity => entity.type === 'text_link' ? entity.url : text.substring(entity.offset, entity.offset + entity.length));

      for (const url of urls) {
        if (url.includes("tiktok.com")) {
          console.log("TikTok URL detected:", url);
          ctx.url = url;
          return true; // TikTok URL found, proceed to handle the post
        }
      }
    }
    return false; // No TikTok URL found in the message
  },
  //nonDeletingRateLimiter,
  handleTiktokPost
);

module.exports = bot;
