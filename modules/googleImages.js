const axios = require("axios");
const fs = require("fs");
const path = require("path");
var Scraper = require('images-scraper');
const google = require('googlethis');

const Markup = require("telegraf/markup");
const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { getRandomInt, generalRateLimiter } = require("../utils");

const bot = new Composer();



// eslint-disable-next-line camelcase
const saved_messages_with_photos = {};

/**
 * @param inputArr
 */
function extractImages(inputArr) {
  let foundarr = null;

  for (const item1lvl of inputArr) {
    if (JSON.stringify(item1lvl).includes("GRID_STATE0")){
      foundarr = item1lvl;
    }
    // if (Array.isArray(item1lvl) === false) {
    //   continue;
    // }
    // for (const item2lvl of item1lvl) {
    //   if (Array.isArray(item2lvl) === false) {
    //     continue;
    //   }
    //   for (const item3lvl of item2lvl) {
    //     if (item3lvl && item3lvl[0] === "GRID_STATE0") {
    //       foundarr = item3lvl;
    //       break;
    //     }
    //   }
    // }
  }

  console.log("found:", foundarr);

  const imagearr = foundarr[0][0][
    foundarr[0][0].findIndex(x => Array.isArray(x) && JSON.stringify(x).includes("GRID_STATE0"))
  ][2][0];

  console.log('found images:', imagearr.length);

  const images = imagearr
    .map(x => {
      // console.log('image:', JSON.stringify(x));
      try{
        return x[0][0][ Object.keys(x[0][0])[0] ][1][3][0];
      } catch(ex){
        console.log('error while getting image')
        return null;

      }
      
    })
    .filter(x => x != null && x.indexOf("http") !== -1);

  return images;
}

/**
 * @param key
 * @param safe
 */
async function getPictureByKeys(key, safe = false) {


  // console.log('starting to find images for :', key);

  const google = new Scraper({
    puppeteer: {
      headless: true,
      args: ['--no-sandbox', '--disable-setuid-sandbox']
  
    },
    safe: false
  });
  

  const results = await google.scrape(key, 100);

  console.log('results', results);

  

  // //

  // // https://www.google.ru/search?q=google+images+parser&tbm=isch
  // let cookie = "";

  // if (safe) {
  //   cookie = "NID=78=NU42mV7XN8ISKCdBMB0fl93m5CFCt7iXvqbNUZul1SJX01yYIYR-MFdGan1sJ59OMY75QZQqjyn3i88jHH-qLX9VQ72p6Dp6z28Fh1BrR4cfgMPLxSuEULn3Yj3-f9-Tw0Zsn0708me2wiixK0YW5MG5rLQj5JpydcJjH9zXIJzMpg";
  // }
  // let result;
  // try {
  //   result = await axios({
  //     url: `https://www.google.com/search?q=${encodeURI(key)}&tbm=isch`,
  //     headers: {
  //       "User-Agent":
  //         "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.82 Safari/537.36",
  //       Cookie: cookie
  //     },
  //     responseType: "text"
  //   });
  // } catch (ex){
  //   console.log('google request failed', ex);
  //   return {status: 'error', reason: ex};
  // }


  // console.log('request completed');

  // const body = result.data;
  // console.log('google send string', body.length);

  // fs.writeFileSync("googleimages.txt", body, err => {
  //   if (err) {
  //     return console.log(err);
  //   }
  // });

  // if (body.includes("Не найдено ни одного изображения по запросу")) {
  //   return { status: "no_image" };
  // }

  // const extractRegex = /AF_initDataCallback\({key: 'ds:1', hash: '\d', data:(.*), sideChannel: {}}\);<\/script><script/gmis;

  // // return {};

  // let jsonMessage = extractRegex.exec(body)[1];

  // console.log("jsonmessage:", jsonMessage);
  // fs.writeFile("jsonmessage.txt", jsonMessage, err => {
  //   if (err) {
  //     return console.log(err);
  //   }
  // });

  // jsonMessage = JSON.parse(jsonMessage);

  // const images = extractImages(jsonMessage);

  const images = results.map(x => x.url);

  console.log("Images received:", images.length);

  const url = images[getRandomInt(0, images.length - 1 < 20 ? images.length - 1 : 20)];

  console.log("selected image:", url);

  return { status: "success", url, images };
}

const getPictureByKeysV2 = async (key, safe = false) => {
  const imagesFromGoogle = await google.image(key, { safe: safe });
  console.log(imagesFromGoogle); 

  const images = imagesFromGoogle.map(x => x.url);

  console.log("Images received:", images.length);

  const url = images[getRandomInt(0, images.length - 1 < 20 ? images.length - 1 : 20)];

  console.log("selected image:", url);

  return { status: "success", url, images };
}

bot.hears(/^(покажи )/i, generalRateLimiter, async (ctx, next) => {

  // console.log('command', ctx.state.command);
  const fullMessageText = ctx.message.text.toLowerCase();

  const searchKey = fullMessageText.replace("покажи ", "");

  ctx.replyWithChatAction("upload_photo");
  console.log("received key for google search:", searchKey);

  const getResult = await getPictureByKeysV2(searchKey);

  if (getResult.status === "success" && getResult.images.length > 0) {
    if (getResult.url.indexOf(".gif") !== -1) {
      ctx
        .replyWithDocument(
          getResult.url,
          Extra.inReplyTo(ctx.message.message_id)
        )
        .catch(() => urlFallback(ctx, getResult.url));
    } else {
      ctx
        .replyWithPhoto(getResult.url, Extra.inReplyTo(ctx.message.message_id))
        .catch(() => urlFallback(ctx, getResult.url));
    }
  }
});

bot.hears(/^(выдача )/i, generalRateLimiter, async (ctx, next) => {

  // console.log('command', ctx.state.command);
  const fullMessageText = ctx.message.text.toLowerCase();

  const searchKey = fullMessageText.replace("покажи ", "");

  ctx.replyWithChatAction("upload_photo");
  console.log("received key for google search:", searchKey);

  const getResult = await getPictureByKeys(searchKey);

  if (getResult.status === "success") {

    // we need to receive first picture from that array, because thats our first try //
    const firstUrl = getResult.images[0];

    console.log(firstUrl);

    // const id = makeid()
    const text = generateMessageForListing({
      currentImageIndex: 0,
      totalImagesCount: getResult.images.length,
      query: searchKey
    });

    ctx
      .replyWithPhoto(
        firstUrl,
        Extra.markdown().caption(text).inReplyTo(ctx.message.message_id).markup(m => m.inlineKeyboard([
          m.callbackButton("<<<<", "prev_img"),
          m.callbackButton(">>>>", "next_img")
        ]))
      )
      .then(result => {
        console.log("result of save:", result);
        saved_messages_with_photos[`${result.chat.id}_${result.message_id}`] = {
          current_pic: 0,
          max_pics: getResult.images.length,
          items: getResult.images,
          query: searchKey
        };
      });
  }
});

bot.on("callback_query", (ctx, next) => {
  console.log("callback_query", ctx.callbackQuery);

  if (
    ctx.callbackQuery.data === "next_img" ||
    ctx.callbackQuery.data === "prev_img"
  ) {
    const save = saved_messages_with_photos[
      `${ctx.callbackQuery.message.chat.id
      }_${ctx.callbackQuery.message.message_id}`
    ];

    if (ctx.callbackQuery.data === "prev_img" && save.current_pic === 0) {
      ctx.answerCbQuery("Ты чё, ебобо? Это первая картинка, иди отсюда.");
    } else if (
      ctx.callbackQuery.data === "next_img" &&
      save.current_pic === save.items.length - 1
    ) {
      ctx.answerCbQuery("Ты чё, ебобо? Это последняя картинка, иди отсюда.");
    } else {
      let newNum = 0;

      if (ctx.callbackQuery.data === "prev_img") {
        newNum = save.current_pic - 1;
      }
      if (ctx.callbackQuery.data === "next_img") {
        newNum = save.current_pic + 1;
      }
      save.current_pic = newNum;
      const text = generateMessageForListing({
        currentImageIndex: newNum,
        query: save.query,
        totalImagesCount: save.items.length
      });

      const extra = Extra.markdown().markup(
        Markup.inlineKeyboard([
          Markup.callbackButton("<<<<", "prev_img"),
          Markup.callbackButton(">>>>", "next_img")
        ])
      );

      ctx.editMessageCaption(`${text}\n*ИДЁТ ЗАГРУЗКА*`, extra);

      ctx
        .editMessageMedia({ type: "photo", media: save.items[newNum] }, extra)
        .then(() => ctx.editMessageCaption(text, extra))
        .catch(() => {
          console.log("cannot be loaded");
          ctx
            .editMessageMedia(
              {
                type: "photo",
                media: {
                  source: fs.createReadStream(
                    path.join(__dirname, "/../assets/cannot-be-loaded.png")
                  )
                }
              },
              extra
            )
            .then(() => ctx.editMessageCaption(text, extra));
        });

      ctx.answerCbQuery();
    }
  }

  next();
});

/**
 * @param root0
 * @param root0.currentImageIndex
 * @param root0.query
 * @param root0.totalImagesCount
 */
function generateMessageForListing({
  currentImageIndex,
  query,
  totalImagesCount
}) {
  return `Запрос *${query}*\nПоказана картинка ${currentImageIndex} из ${totalImagesCount}. `;
}

const urlFallback = (ctx, link) => {
  ctx.replyWithMarkdown(
    `[Ссылка](${link})`,
    Extra.inReplyTo(ctx.message.message_id)
  );
};

module.exports = bot;
