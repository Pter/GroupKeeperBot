const mongoose = require("mongoose");

const donateSchema = new mongoose.Schema({
  rawMessage: {
    type: mongoose.Schema.Types.Mixed
  }
}, { timestamps: true });

const donateModel = mongoose.model("donate", donateSchema);

module.exports = donateModel;
