const mongoose = require("mongoose");
const config = require("./config");

/**
 *
 */
async function connect() {
  return mongoose.connect(config.mongoUri, { useNewUrlParser: true });
}

module.exports = connect;
