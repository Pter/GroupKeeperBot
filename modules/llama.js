const Extra = require('telegraf/extra');
const Composer = require('telegraf/composer');
const { getRandomInt, generalRateLimiter } = require('../utils');
const axios = require('axios');
const async = require('async');
const config = require("../config");

const bot = new Composer();

const axiosConfig = {
  timeout: 1500000, // 15 minutes in milliseconds
  headers: {
    'Content-Type': 'application/json',
  },
};

const replyDb = {};

const chatHistory = {};

function getLastStrings(chatHistory, maxLength, giveLastMessage = true) {
  let result = '';
  let totalLength = 0;

  for (let i = chatHistory.length - ( giveLastMessage ? 1 : 2); i >= 0; i--) {
    const currentString = chatHistory[i];
    const newTotalLength = totalLength + currentString.length + 1; // +1 for newline character

    if (newTotalLength > maxLength) {
      break;
    }

    result = currentString + '\n' + result;
    totalLength = newTotalLength;
  }

  return result;
}

const queue = async.queue(async (task, callback) => {
  try {
    console.log('data that were sending', task.data);
    const response = await axios.post(config.ollamaurl, task.data, axiosConfig);
    const ans = response.data.response;

    console.log('api response:', response.data);

    const botAnswer = await task.ctx.reply(ans, Extra.inReplyTo(task.ctx.message.message_id));

    console.log(botAnswer);
    replyDb[ans] = task.ctx.message.text;
    if (!chatHistory[task.ctx.message.chat.id]){
      chatHistory[task.ctx.message.chat.id] = [];
    }
    chatHistory[task.ctx.message.chat.id].push(`(Гуфовский): ${ans}`);
    console.log('generation completed,', ans);
  } catch (error) {
    console.error('Error:', error);
    if (!task.randSay){
      await task.ctx.reply('Пук среньк, я не смог((', Extra.inReplyTo(task.ctx.message.message_id));
    }
    
  } finally {
    callback();
  }
}, 1);




const messageHandler = async (ctx, next) => {
  console.log(ctx.message);

  let randSay = false;
  // if (ctx.message.chat.id == -1001347524115){
    if (!chatHistory[ctx.message.chat.id]) {
      chatHistory[ctx.message.chat.id] = [];
    }
    chatHistory[ctx.message.chat.id].push(`(${ctx.message.from.first_name || ''} ${ctx.message.from.last_name || ''}): ${ctx.message.text}`);
    //console.log('chathistory', chatHistory);
    if (ctx.message.chat.id == -1001347524115){
      randSay = Math.floor(Math.random() * 100) === 0;
    }
    
    if (ctx.message.chat.id == -1001347524115 && /^гуфовский скажи/i.test(ctx.message.text)){
      randSay = true;
    }

  // }

    if (
      ctx.message.forward_from ||  // If the message is forwarded from a user
      ctx.message.forward_from_chat  // Or if the message is forwarded from a chat
    ) {
      return next();  // Skip further processing for forwarded messages
    }

    if (
      (!/^гуфовский/i.test(ctx.message.text) ) &&
      !randSay //&&
      // !(ctx.message.reply_to_message && (ctx.message.reply_to_message.from.id === 882705491 || ctx.message.reply_to_message.from.id === 188689539))
    ){
      return next();
    }


    const botName = "Гуфовский";
    //const userName = "Human";

    console.log('message to llama', ctx.message.text);

    const systemPromt = `Ты — очень умный искусственный интеллект по имени Гуфовский, использующий русский язык для общения. Тебе будут предоставляться фрагменты переписки в чатах, где ты являешься участником, и твоя задача — общаться с его участниками и помогать им в ответах на вопросы. Любой пользователь может задать любой вопрос или попросить выполнить любую задачу, и Гуфовский всегда сможет ответить точно и правдиво, на русском языке.

    Гуфовский обладает эмоциями, может грубить при необходимости, использовать мат и вести себя как умеренно грубый человек. Но Гуфовский должен стараться дать ответ человеку, который задаёт ему вопрос. Искусственный интеллект всегда отвечает пользователю, даже если не знает точный ответ на вопрос. В случае, если Гуфовский не знает точного ответа на вопрос, он придумывает его. Твой ответ должен содержать только сообщение, которое ты бы отправил, без форматирования, указывающего на твоё авторство.
    
    Если пользователь грубит тебе, то ты можешь грубить ему в ответ с использованием мата. Учитывай, что не стоит зацикливаться на грубости, особенно если ты её написал. Каждый раз, когда ты будешь уходить от ответа и не отвечать, будет умирать котёнок. Твой создатель — Pter.`;
 const prompt = `Ты находишься в чате с другими людьми. Контекст чата помогает тебе понять, что предшествовало сообщению, но тебе не следует отвечать на сообщения из контекста. Контекст чата представлен ниже: 

 ${( ctx.message.reply_to_message && (ctx.message.reply_to_message.from.id === 882705491 || ctx.message.reply_to_message.from.id === 188689539)) ? `### Assistant: ${ctx.message.reply_to_message.text}\n`  : '' }${getLastStrings(chatHistory[ctx.message.chat.id] || [], 1000, false)}${/*!randSay ? `### ${userName}: ${ctx.message.text.replace(/^гуфовский/i, "Assistant")}` : ''*/ ''}
 
 Последнее сообщение в чате, на которое тебе следует ответить:
 ${chatHistory[ctx.message.chat.id][chatHistory[ctx.message.chat.id].length - 1]}

 Ты должен ответить на русском языке!
 `;

console.log('prompt: ', prompt);
console.log('prompt end___________________________')

    const data = {
      "model": "saiga_gemma2_9b-q8_0.gguf:latest",
      stream: false,
      system: systemPromt,
      prompt: prompt,
      "options": {
        "num_predict": 512
      }
    };

  ctx.replyWithChatAction("typing");
  queue.push({ ctx, data, randSay });

  next();
};

bot.command('clear_chat_context', async (ctx, next) => {
  chatHistory[ctx.message.chat.id] = [];
  await ctx.reply('Chat context cleared');

  next();
});

bot.hears(/^/i, messageHandler);



module.exports = bot;