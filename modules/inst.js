const Composer = require("telegraf/composer");
const { Extra } = require("telegraf");
const { downloadBuffer } = require("../utils");

const bot = new Composer();

// Function to fetch video URL or download video using Fallback API
async function fetchFallbackInstagramVideoUrl(instagramUrl) {
  const { got } = await import("got");
  const fallbackEndpoint = `https://instagram-videos.vercel.app/api/video?postUrl=${encodeURIComponent(instagramUrl)}`;
  try {
    const response = await got(fallbackEndpoint, { responseType: 'json' });
    if (response.body.status === 'success' && response.body.data.videoUrl) {
      return { success: true, url: response.body.data.videoUrl };
    } else {
      console.log('Fallback API did not return a video URL:', response.body.message);
      return { success: false, message: 'Failed to get video URL from fallback API' };
    }
  } catch (error) {
    console.error('Error calling Fallback API:', error);
    return { success: false, message: 'Error calling Fallback API' };
  }
}

// Function to fetch video URL or download video using Cobalt API
async function fetchInstagramVideoUrl(instagramUrl) {
  const { got } = await import("got");
  const cobaltEndpoint = 'https://co.wuk.sh/api/json';
  try {
    const response = await got.post(cobaltEndpoint, {
      json: { url: instagramUrl },
      responseType: 'json',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    if (['success', 'redirect'].includes(response.body.status) && response.body.url) {
      return { success: true, url: response.body.url };
    } else if (response.body.status === 'picker'){
      const video = response.body.picker.find(x => x.type === 'video');
      if (!video){
        console.log('there is no video');
        return;
      }
      return { success: true, url: video.url };
    } else {
      console.log('Cobalt API did not return a video URL:', response.body);
      // If Cobalt API fails, try the fallback API
      return await fetchFallbackInstagramVideoUrl(instagramUrl);
    }
  } catch (error) {
    console.error('Error calling Cobalt API:', error);
    // If Cobalt API fails, try the fallback API
    return await fetchFallbackInstagramVideoUrl(instagramUrl);
  }
}

// Handler for Instagram posts
const handleInstagramPost = async (ctx, next) => {
  if (!ctx.url) {
    console.log('No Instagram URL found in the message');
    return next();
  }

  try {
    const { success, url, message } = await fetchInstagramVideoUrl(ctx.url);
    if (success) {
      await ctx.replyWithVideo(url, Extra.inReplyTo(ctx.message.message_id));
    } else {
      console.log(message);
      // Uncomment the following line to send a reply when the video cannot be processed
      // ctx.reply('Unable to process the Instagram video.', Extra.inReplyTo(ctx.message.message_id));
    }
  } catch (error) {
    console.error('Error handling Instagram post:', error);
    // Uncomment the following line to send a reply when an error occurs
    // ctx.reply('An error occurred while processing the Instagram video.', Extra.inReplyTo(ctx.message.message_id));
  }
};

// Detection of Instagram URLs in messages
bot.hears(
  async (text, ctx) => {
      if (ctx.message.entities) {
          const urls = ctx.message.entities
              .filter((entity) => entity.type === 'url' || entity.type === 'text_link')
              .map((entity) => entity.type === 'text_link' ? entity.url : text.substring(entity.offset, entity.offset + entity.length));

          for (const url of urls) {
              if (url.includes('instagram.com')) {
                  console.log("Instagram URL detected:", url);
                  ctx.url = url;
                  return true; // Instagram URL found, proceed to handle the post
              }
          }
      }
      return false; // No Instagram URL found in the message
  },
  handleInstagramPost
);

module.exports = bot;
