const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");
const fs = require('fs');
const { Canvas, createCanvas, Image, ImageData, loadImage } = require('canvas');
const { JSDOM } = require('jsdom');
const cv = require('../libs/opencv.js')

function installDOM() {
  const dom = new JSDOM();
  global.document = dom.window.document;
  // The rest enables DOM image and canvas and is provided by node-canvas
  global.Image = Image;
  global.HTMLCanvasElement = Canvas;
  global.ImageData = ImageData;
  global.HTMLImageElement = Image;
}

installDOM();
// const cv = require("opencv4nodejs");
// const cv = {};
const axios = require("axios");
const Jimp = require("jimp");
// const cv = require('@techstark/opencv-js');

// console.log(glfx());

// const { generalRateLimiter } = require('../utils')

const bot = new Composer();
// console.log('cv', cv)
// process.exit(1)

let classifier;


cv['onRuntimeInitialized'] = () => {
  classifier = new cv.CascadeClassifier()
  const xmlString = fs.readFileSync('./haarcascade_frontalcatface_extended.xml', 'utf-8');

  // Convert the string to an ArrayBuffer
  const xmlArrayBuffer = new TextEncoder().encode(xmlString).buffer;
  const data = new Uint8Array(xmlArrayBuffer);
  cv.FS_createDataFile('/', 'classifier.xml', data, true, false, false);
  classifier.load('/classifier.xml');
};



// const classifier = cv.CascadeClassifier(
//   "./haarcascade_frontalcatface_extended.xml"
// );

bot.on(["photo", "text"], async(ctx, next) => {
  let itsCat = false;
  if (
    (ctx.message.caption && ctx.message.caption.startsWith("/cat") ) ||
    (ctx.message.text && ( ctx.message.text.startsWith("/cat")) &&
      ctx.message.reply_to_message &&
      ctx.message.reply_to_message.photo)
  ) {
    itsCat = true;
    // it's ok
  } else {
    // return next();
  }
  let vipukCoeff = 20;
  if (
    (ctx.message.caption && ctx.message.caption.toLowerCase().startsWith("выпукни")) ||
    (ctx.message.text && ( ctx.message.text.toLowerCase().startsWith("выпукни")) &&
      ctx.message.reply_to_message &&
      ctx.message.reply_to_message.photo)
  ) {
    let str = ctx.message.caption || ctx.message.text;
    if (str){
      let maybeint = str.split(" ")[1];
      maybeint = parseInt(maybeint);
      if (maybeint > 0 && maybeint < 100){
        vipukCoeff = maybeint;
      }
    }
    // it's ok
  } else {
    if (!itsCat)
      return next();
  }
  console.log('itsCat:', itsCat);

  console.log("cat:", ctx);
  let photoArray;

  if (Array.isArray(ctx.message.photo) && ctx.message.photo.length > 0) {
    photoArray = ctx.message.photo;
  } else if (
    Array.isArray(ctx.message.reply_to_message.photo) &&
    ctx.message.reply_to_message.photo.length > 0
  ) {
    photoArray = ctx.message.reply_to_message.photo;
  } else {
    return next();
  }

  const photo = photoArray.pop();
  const imageUrl = await ctx.telegram.getFileLink(photo.file_id);

  console.log("ImageUrl", imageUrl);

  const httpResponse = await axios.get(imageUrl, { responseType: "arraybuffer" });

  console.log("received image data", httpResponse.data);


  const imageCanvas = await loadImage(httpResponse.data);
  const image = cv.imread(imageCanvas);


  const gray = new cv.Mat()
  cv.cvtColor(image, gray, cv.COLOR_RGBA2GRAY)
  

  

  let bulgeRadius;
  let bulgeStrength;

  let cx;
  let cy; 

  // const outImageBuffer = cv.imencode(".jpg", image);


  if (itsCat){
    // Detect cat faces in the image
    const catFaces = new cv.RectVector()
    const size = new cv.Size(0, 0)
    classifier.detectMultiScale(gray, catFaces, 1.05, 3, 0, size, size)
    
    // Convert the detected cat faces to an array of objects
    const catFaceObjects = []
    for (let i = 0; i < catFaces.size(); i++) {
      const faceRect = catFaces.get(i)
      catFaceObjects.push({
        x: faceRect.x,
        y: faceRect.y,
        width: faceRect.width,
        height: faceRect.height
      })
    }
  
    console.log("cat_face", catFaceObjects);
  
    if (catFaceObjects.length === 0) {
      return ctx.reply(
        "Cats not found :c",
        Extra.inReplyTo(ctx.message.message_id)
      );
    }
  
    // for (const obj of catFace.objects) {
    //   image.drawRectangle(
    //     new cv.Point2(obj.x, obj.y),
    //     new cv.Point2(obj.x + obj.width, obj.y + obj.height),
    //     new cv.Vec3(0, 127, 255),
    //     3
    //   );
    // }

    

    // console.log("outImageBuffer", outImageBuffer);

    // ctx.replyWithPhoto(
    //   { source: outImageBuffer },
    //   Extra.inReplyTo(ctx.message.message_id)
    // );

    // const ctx = gl(photo.width, photo.height);

    const selectedCatFace = catFaceObjects[0];

    if (!selectedCatFace) {
      console.log("NOCATFACE");
      return;
    }

    cx = selectedCatFace.x + selectedCatFace.width / 2;
    cy = selectedCatFace.y + selectedCatFace.height / 2;

    console.log("cx:", cx);
    console.log("cy:", cy);

    bulgeRadius = Math.max(selectedCatFace.width, selectedCatFace.height);
    bulgeStrength = 1;
  } else {
    cx = Math.round(photo.width / 2);
    cy = Math.round(photo.height / 2);

    bulgeRadius = Math.min(photo.height * vipukCoeff * 0.01, photo.width * vipukCoeff * 0.01);
    bulgeStrength = 1;
  }



  const w = photo.width;
  const h = photo.height;

  const jimpedImage = await Jimp.read((ctx.message.caption || ctx.message.text).includes("debug") ? outImageBuffer : httpResponse.data);

  const targetImage = await Jimp.read(httpResponse.data);

  // console.log('jimpedImage', jimpedImage)
  // console.log('targetImage', targetImage)

  for (let x = 0; x < w; x++) {
    for (let y = 0; y < h; y++) {
      const dx = x - cx;
      const dy = y - cy;
      const distanceSquared = dx * dx + dy * dy;
      let sx = x;
      let sy = y;

      if (distanceSquared < bulgeRadius * bulgeRadius) {
        const distance = Math.sqrt(distanceSquared);
        let otherMethod = false;

        otherMethod = true;
        if (otherMethod) {
          const r = distance / bulgeRadius;
          const a = Math.atan2(dy, dx);
          const rn = Math.pow(r, bulgeStrength) * distance;
          const newX = rn * Math.cos(a) + cx;
          const newY = rn * Math.sin(a) + cy;

          sx += newX - x;
          sy += newY - y;
        } else {
          const dirX = dx / distance;
          const dirY = dy / distance;
          const alpha = distance / bulgeRadius;
          const distortionFactor = distance * Math.pow(1 - alpha, 1.0 / bulgeStrength);

          sx -= distortionFactor * dirX;
          sy -= distortionFactor * dirY;
        }
      }
      if (sx >= 0 && sx < w && sy >= 0 && sy < h) {
        const pixelColor = jimpedImage.getPixelColor(sx, sy);

        targetImage.setPixelColor(pixelColor, x, y);

        // output.setRGB(x, y, rgb);
      }
    }
  }

  console.log("cycle end");
  const jimpedBuffer = await targetImage.getBufferAsync("image/jpeg");

  console.log("jimpedBuffer", jimpedBuffer);
  ctx.replyWithPhoto(
    { source: jimpedBuffer },
    Extra.inReplyTo(ctx.message.message_id)
  );
});

module.exports = bot;
