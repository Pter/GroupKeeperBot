console.log('Initializing...');
const config = require("./config.json"); // init config

const Sentry = require("@sentry/node");
// or use es6 import statements
// import * as Sentry from '@sentry/node';

const Tracing = require("@sentry/tracing");
// or use es6 import statements
// import * as Tracing from '@sentry/tracing';

Sentry.init({
  dsn: config.sentry,

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});


process.on('uncaughtException', (error) => {
  console.error('Uncaught exception:', error);
  // Consider gracefully shutting down here
});



const connectToDatabase = require("./database");
const { uploadFile, initAll } = require('./tgUpload');
const { Telegraf } = require("telegraf");
const commandParts = require("telegraf-command-parts");

console.log('Loading modules');
const modules = require("./modules");
console.log('Modules loaded');

const Message = require("./models/message");

const bot = new Telegraf(config.token);

bot.use(commandParts());


bot.hears('aboba', async (ctx, next) => {
  try {
    console.log('aaaaploads')
    const fileId = await uploadFile('', -463512995);
    console.log('uploadedFileId', fileId);
    ctx.replyWithDocument(new String(fileId));
  } catch (ex) {
    console.log('fatal ', ex);
  } finally {
    next();
  }
})

bot.start(ctx => ctx.reply("Дарова! Я ГрупКиперБот, и я буду вас развлекать!"));

bot.use((ctx, next) => {
  if (ctx.message) {
    console.log("message", ctx.message);
    const dbMessage = new Message({ rawMessage: ctx.message });

    dbMessage.save();
  }

  next();
});

bot.catch((err, ctx) => {
  console.error(`Ooops, encountered an error for ${ctx.updateType}`, err)
})

for (const module of modules) {
  bot.use(module);
}


startUp();

async function startUp() {
  console.log('Connecting to database...');
  await connectToDatabase();
  console.log('Database connected');
  await initAll();
  bot.launch();
}


