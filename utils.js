const axios = require("axios");
const rateLimit = require("telegraf-ratelimit");

/**
 * @param min
 * @param max
 */
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * @param term
 */
async function getRandomPicByTerm(term) {
  const result = await axios({
    url: `https://api.unsplash.com/photos/random?client_id=ce5425e5114f5fc05e230813da9198ac20d6abc0c8125cffe75a2f0f1552ceb5&query=${term}`
  });

  return result.data.urls.regular;
}

async function downloadBuffer(url, headers = {}) {
  const result = await axios({url, responseType: 'arraybuffer', headers,  validateStatus: function (status) {
    return status < 500; // Resolve only if the status code is less than 500
  }});
  return result.data;
}

/**
 *
 */
function makeid() {
  let text = "";
  const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (let i = 0; i < 5; i += 1) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

const limitConfig = {
  window: 5000,
  limit: 1,
  onLimitExceeded: ctx => ctx.deleteMessage(ctx.message.message_id)
};

/**
 * @param root0
 * @param root0.deleteMessage
 */
function createGeneralRateLimiter({ deleteMessage } = { deleteMessage: true }) {
  if (deleteMessage) {
    return rateLimit(limitConfig);
  }
  return rateLimit(
    Object.assign(limitConfig, { onLimitExceeded: () => {} })
  );
}

module.exports = {
  getRandomInt,
  getRandomPicByTerm,
  makeid,
  createGeneralRateLimiter,
  generalRateLimiter: createGeneralRateLimiter({ deleteMessage: true }),
  nonDeletingRateLimiter: createGeneralRateLimiter({ deleteMessage: false }),
  downloadBuffer
};
