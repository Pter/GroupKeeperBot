const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");
const Markup = require("telegraf/markup");
const Donate = require("../models/donate");

const { getRandomPicByTerm, generalRateLimiter } = require("../utils");

const bot = new Composer();

const handler = async (ctx, next) => {
  ctx.replyWithMarkdown(
    `You may support development of this bot by donating to me on [buymeacoffee](https://www.buymeacoffee.com/pterko). This will help me improve and maintain this bot and other projects. Thank you!`,
    Extra.inReplyTo(ctx.message.message_id).markup(
      Markup.inlineKeyboard([
        [Markup.callbackButton("Donate via Telegram (25⭐️)", "donate_telegram_small")],
        [Markup.callbackButton("Donate via Telegram (50⭐️)", "donate_telegram_middle")],
        [Markup.callbackButton("Donate via Telegram (200⭐️)", "donate_telegram_big")],
      ])
    )
  );
  next();
};

const invoiceHandler = async (ctx) => {
  console.log(ctx);
  const payload = "donate_payload"; // Unique payload for the invoice
  const prices = []; // Amount in the smallest units of currency, e.g., 5000 kopecks to equal 50 rubles

  if (ctx.match === "donate_telegram_small") {
    prices.push({ label: "Small Donation", amount: 25 });
  }
  if (ctx.match === "donate_telegram_middle") {
    prices.push({ label: "Nice Donation", amount: 50 });
  }
  if (ctx.match === "donate_telegram_big") {
    prices.push({ label: "Big Donation", amount: 200 });
  }

  await ctx.replyWithInvoice({
    title: "Support Donation",
    description: "Support the development and maintenance of this bot",
    payload: payload,
    provider_token: "", // Replace with your actual provider token
    //start_parameter: "donate",
    currency: "XTR", // Change to your desired currency
    prices: prices,
    // need_name: false, // Optional: Request name
    // need_email: false, // Optional: Request email
    // need_shipping_address: false, // Optional: Request shipping address
    //is_flexible: true // Optional: Allow users to choose the amount
  });
};

bot.command("donate", generalRateLimiter, handler);
bot.action("donate_telegram_small", invoiceHandler);
bot.action("donate_telegram_middle", invoiceHandler);
bot.action("donate_telegram_big", invoiceHandler);

bot.on("pre_checkout_query", (ctx) => {
  ctx.answerPreCheckoutQuery(true);
  Donate.create({ rawMessage: ctx.update });
});

module.exports = bot;
