const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { getRandomPicByTerm, generalRateLimiter } = require("../utils");

const bot = new Composer();

const handler = searchTerm => async(ctx, next) => {
  const link = await getRandomPicByTerm(searchTerm || "cat");

  ctx.replyWithMarkdown(
    `[Ссылка](${link})`,
    Extra.inReplyTo(ctx.message.message_id)
  );
  next();
};

bot.hears("запости котика", generalRateLimiter, handler("cat"));
bot.hears("запости енотика", generalRateLimiter, handler("racoon"));
bot.hears("запости лисика", generalRateLimiter, handler("fox"));

module.exports = bot;
