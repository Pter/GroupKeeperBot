const {Api, TelegramClient} = require("telegram");
const { CustomFile } = require("telegram/client/uploads");
const { StringSession } =require ("telegram/sessions");
const fs = require('fs');


const config = require("./config.json"); // init config

const stringSession = config.sessionString || ''; // leave this empty for now
const BOT_TOKEN = config.token; // put your bot token here

const apiId = config.apiId;
const apiHash = config.apiHash;

let savedClient;


const initAll = async () => {
  try{
    const client = new TelegramClient(new StringSession(stringSession), apiId, apiHash, {connectionRetries: 5});
  
    await client.start({
      botAuthToken: BOT_TOKEN 
    });
    console.log(client.session.save())
    savedClient = client;
  } catch(ex){
    console.error('Mtproto error:', ex);
  }

}




const uploadFile = async (fileBuffer, chatId) => {
  console.log('uploadFileFor:', chatId);
  await savedClient.connect(); // This assumes you have already authenticated with .start()

  const toUpload = new CustomFile("testvid.mp4", fs.statSync("testvid.mp4").size, "testvid.mp4");
  const file = await savedClient.uploadFile({
    file: toUpload,
    workers: 1,
  });


  // await savedClient.invoke(new Api.photos.UploadProfilePhoto({
  //     file: file,
  // }));

  // const result = await savedClient.sendFile(chatId,{
  //     file: "testvid.mp4",
  // });

  // console.log('file', file);
  // console.log('id:', file.id.value);
  console.log('file', file);

  return file;

  // const result = await savedClient.invoke(
  //   new Api.messages.UploadMedia({
  //     peer: chatId,
  //     media: new Api.InputMediaDocument({
  //       id: await savedClient.uploadFile({
  //         file: new CustomFile(
  //           "testvid.mp4",
  //           fs.statSync("testvid.mp4").size,
  //           "testvid.mp4"
  //         ),
  //         workers: 1,
  //       }),
  //       ttlSeconds: 43,
  //     }),
  //   })
  // );
  // console.log(result); // prints the result
}

const getClient = async () => { 
  if (!savedClient)
    return;
  await savedClient.connect();

  return savedClient;
}

module.exports = {
  initAll,
  uploadFile,
  getClient,
}