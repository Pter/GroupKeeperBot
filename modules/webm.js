const fs = require("fs");
const path = require("path");
const download = require("download");

const prettyFrameRate = require("pretty-frame-rate");
const ffprobe = require("ffprobe");
const ffprobeStatic = require("ffprobe-static");
const ffmpeg = require("fluent-ffmpeg");
const pathToFfmpeg = require("ffmpeg-static");

ffmpeg.setFfmpegPath(pathToFfmpeg);

const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { makeid, nonDeletingRateLimiter } = require("../utils");

const bot = new Composer();

const handleWebm = async(ctx, next) => {
  console.log("start download");
  const startDownload = Date.now();


  // const filePath = await bot.downloadFile(ctx.document.file_id, path.join(__dirname + "../downloads_webm"));
  if (!ctx.url) {
    const fileLink = await ctx.telegram.getFileLink(
      ctx.message.document.file_id
    );

    console.log("file:", fileLink);
    ctx.url = fileLink;
  }

  const fileFolder = path.join(__dirname, "../downloads_webm/");
  const fileName = ctx.message.document
    ? makeid() + ctx.message.document.file_name
    : `${makeid()}.webm`;
  const filePath = path.join(fileFolder, fileName);

  console.log("filePath", filePath);
  await download(ctx.url, fileFolder, {
    filename: fileName
  });

  const endDownload = Date.now();

  console.log("downloaded file path:", filePath);
  const probe = await ffprobe(filePath, { path: ffprobeStatic.path });

  // we need to find needed stream:
  const selectedVideoStream = probe.streams
    .filter(x => x.codec_type === "video")
    .sort((a, b) => {
      if (a.tags.DURATION > b.tags.DURATION) {
        return 1;
      }
      if (a.tags.DURATION < b.tags.DURATION) {
        return -1;
      }
      return 0;
    })
    .pop();

  console.log("selectedVideoStream;", selectedVideoStream);

  const maxWidth = 1280;
  const maxFPS = 60;
  const maxFramesCount = 36000;

  const calculatedFps = parseFloat(
    prettyFrameRate(selectedVideoStream.avg_frame_rate, { suffix: "" })
  ) <= maxFPS
    ? selectedVideoStream.avg_frame_rate
    : maxFPS;

  const startTime = Date.now();

  await ffmpeg()
    .input(filePath)
    .format("mp4")
    .size(
      `${
        selectedVideoStream.width <= maxWidth
          ? selectedVideoStream.width
          : maxWidth
      }x?`
    )
    .outputOptions([
      "-preset veryfast",
      "-pix_fmt yuv420p",
      "-max_muxing_queue_size 999999",
      "-movflags +faststart",
      "-crf 23",
      "-c:v libx264"
    ])
    .fps(calculatedFps)
    .frames(maxFramesCount)
    .on("end", () => {
      console.log("file has been converted succesfully");

      // let message_options = {
      //     reply_to_message_id: ctx.message_id,
      //     supports_streaming: true,
      //     caption: `Render time: ${Date.now() - startTime}ms. Download time: ${endDownload - startDownload}ms`
      // };

      ctx
        .replyWithVideo(
          {
            source: fs.createReadStream(`${filePath}.mp4`)
          },
          Extra.caption(
            `Render time: ${Date.now() -
              startTime}ms. Download time: ${endDownload - startDownload}ms`
          ).inReplyTo(ctx.message.message_id)
        )
        .finally(() => {
          fs.unlink(filePath, () => {});
          fs.unlink(`${filePath}.mp4`, () => {});
        });
    })
    .on("error", err => {
      console.log(`an error happened: ${err.message}`);
    })
    .save(`${filePath}.mp4`);
};

bot.hears(
  (text, ctx) => {
    if (ctx.message.entities) {
      for (const entity of ctx.message.entities.filter(
        x => x.type === "url"
      )) {
        const url = ctx.message.text.substring(
          entity.offset,
          entity.length + entity.offset
        );


        // console.log("url is:", url);
        if (url.endsWith(".webm")) {
          ctx.url = url;
          return true;
        }
      }
    }
    return false;
  },
  nonDeletingRateLimiter,
  handleWebm
);

bot.on(
  "document",
  async(ctx, next) => {
    console.log("handled in webm: ", ctx.message);

    if (ctx.message.document && ctx.message.document.mime_type === "video/webm") {
      return next();
    }
  },
  nonDeletingRateLimiter,
  handleWebm
);

module.exports = bot;
