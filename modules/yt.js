const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");
const fs = require("fs");
const ytdl = require("ytdl-core");
const config = require("../config");

const { nonDeletingRateLimiter, downloadBuffer } = require("../utils");

const bot = new Composer();

function stream2buffer(stream) {
  return new Promise((resolve, reject) => {
    const _buf = [];

    stream.on("data", (chunk) => {
        _buf.push(chunk)
        console.log('cursize:', _buf.length);

    });
    stream.on("end", () => resolve(Buffer.concat(_buf)));
    stream.on("error", (err) => reject(err));
  });
}

const handleYtPost = async (ctx, next) => {
  try {
    const reqOptions = {
      requestOptions: {
        headers: {
          cookie: config.youtube.cookie,
        }
      }
    }
    const info = await ytdl.getBasicInfo(ctx.url, reqOptions);
    console.log("video info:", info);

    const videoDuration = parseInt(info.videoDetails.lengthSeconds);

    if (!(videoDuration > 1 && videoDuration < 90)) return;

    const downloadStream = ytdl(ctx.url, { filter: "videoandaudio", ...reqOptions });

    const buffer = await stream2buffer(downloadStream);

    console.log("Downloaded buffer:", buffer.length);
    const tgResult = await ctx.replyWithVideo(
      {
        source: buffer,
      },
      Extra
      //.caption().HTML()
      .inReplyTo(ctx.message.message_id)
    );
    console.log("tgres:", tgResult);
  } catch (ex) {
    console.log("Error while downloading:", ex);
    //ctx.reply("Хм, произошла ошибка :c ");
  }
};

// https://www.youtube.com/shorts/2tOT0hPfqCI
// https://youtu.be/2tOT0hPfqCI
// https://www.youtube.com/watch?v=2tOT0hPfqCI
bot.hears(
  (text, ctx) => {
    if (!ctx.message.entities)
      return;
    console.log("hello from youtube");

    const eligibleUrlForCheck = ctx.message.entities
      .filter((x) => x.x === "text_link")
      .map((x) => entity.url)
      .concat(
        ctx.message.entities
          .filter((x) => x.type === "url")
          .map((x) => ctx.message.text.substring(x.offset, x.length + x.offset))
      );
    console.log("eligibleUrlForCheck", eligibleUrlForCheck);

    if (eligibleUrlForCheck.length > 0) {
      for (const url of eligibleUrlForCheck) {
        if (
          /^(https?:\/\/)?(www\.)?youtube.com\/shorts\//.test(url) ||
          /^(https?:\/\/)?(www\.)?youtu.be\//.test(url) ||
          /^(https?:\/\/)?(www\.)?youtube.com\/watch\?v=/.test(url)
        ) {
          console.log("yt1 true");
          ctx.url = url;
          return true;
        }
      }
    }
    console.log("yt false");
    return false;
  },
  nonDeletingRateLimiter,
  handleYtPost
);
module.exports = bot;
