const fs = require("fs");
const axios = require("axios");
const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { generalRateLimiter } = require("../utils");

const bot = new Composer();

const staticPicturesHandler = path => (ctx, next) => {
  const catalog = path || "/root/femalefur_madi/";

  fs.readdir(catalog, (err, items) => {
    if (err || !items || items.length === 0) {

      // No found folder
    } else {
      const item = items[Math.floor(Math.random() * items.length)];

      ctx.replyWithPhoto(
        { source: fs.createReadStream(catalog + item) },
        Extra.inReplyTo(ctx.message.message_id)
      );
    }
  });
};

const e621handler = url => async(ctx, next) => {
  const requestResult = await axios({
    url:
      url ||
      "https://e621.net/post/index.json?limit=1&tags=score%3A%3E%3D100+rating%3Asafe+type%3Ajpg+type%3Apng+order%3Arandom+rating%3Asafe",
    headers: {
      hostname: "e621.net",
      host: "e621.net",
      "user-agent": "GroupKeeperBot /fyrporn command",
      cookie:
        "__cfduid=d224a60b351805879731888da895c36251474588690; blacklist_avatars=true; blacklist_users=false; e621=BAh7BzoPc2Vzc2lvbl9pZCIlNjM0ZDM2ZjI1ZjdlOGI3NDIwOTU3NjIxZDE1N2NkMjQ6EF9jc3JmX3Rva2VuSSIxb0tXbm1aeS9jMFVlUGhHRVZNN3B3MHdrZWl0YkFINXFGMXJxeEw1WTN5QT0GOgZFRg%3D%3D--0ee9569376e2ef1419d2749fce0af046f21a03a9; css=hexagon%3Boverrides%2Fspring%3B1462320000; mode=view; _ga=GA1.2.454308874.147458920"
    }
  });

  const link = requestResult.data[0].sample_url || requestResult.data[0].file_url;

  ctx.replyWithMarkdown(
    `[Ссылка](${link})`,
    Extra.inReplyTo(ctx.message.message_id)
  );
};

bot.command(
  "mt",
  generalRateLimiter,
  staticPicturesHandler("/root/femalefur_madi/")
);
bot.command(
  "fyr2",
  generalRateLimiter,
  staticPicturesHandler("/root/cleanfur/")
);

bot.command(
  "fyr",
  generalRateLimiter,
  e621handler(
    "https://e621.net/post/index.json?limit=1&tags=score%3A%3E%3D100+rating%3Asafe+type%3Ajpg+type%3Apng+order%3Arandom+rating%3Asafe"
  )
);
bot.command(
  "fyrporn",
  generalRateLimiter,
  e621handler(
    "https://e621.net/post/index.json?limit=1&tags=score%3A%3E%3D100+type%3Ajpg+type%3Apng+order%3Arandom"
  )
);
bot.command(
  "fyrporngif",
  generalRateLimiter,
  e621handler(
    "https://e621.net/post/index.json?limit=1&tags=score%3A%3E%3D100+rating%3Ae+type%3Agif+order%3Arandom+male%2Ffemale+female"
  )
);

module.exports = bot;
