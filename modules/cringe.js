const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");

const { generalRateLimiter } = require("../utils");

const bot = new Composer();

bot.hears("кринж", generalRateLimiter, (ctx, next) => {
  if (ctx.message.chat.id !== -1001058344951) {
    return next();
  }
  ctx.replyWithSticker(
    "BQADAgAD3gEAAktTzgU8oQHbljFX1gI",
    Extra.inReplyTo(ctx.message.message_id)
  );

  next();
});

module.exports = bot;
