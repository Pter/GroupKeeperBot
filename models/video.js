const mongoose = require("mongoose");

const videoSchema = new mongoose.Schema({
  url: "string"
}, { timestamps: true });

const videoModel = mongoose.model("video", videoSchema);

module.exports = videoModel;
