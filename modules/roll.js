const Extra = require("telegraf/extra");
const Composer = require("telegraf/composer");
const { getRandomInt, generalRateLimiter } = require("../utils");

const bot = new Composer();

const handler = mode => (ctx, next) => {
  let randomInt;

  if (mode === "double") {
    randomInt = getRandomInt(10, 99);
  }
  if (mode === "roll") {
    randomInt = getRandomInt(0, 100);
  }

  ctx.replyWithMarkdown(
    `Ты выбросил ***${randomInt}***`,
    Extra.inReplyTo(ctx.message.message_id)
  );
};

bot.hears(/^(на дабл)/i, generalRateLimiter, handler("double"));
bot.command("roll", generalRateLimiter, handler("roll"));

module.exports = bot;
