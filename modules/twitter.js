const axios = require('axios');
const Extra = require('telegraf/extra');
const Composer = require('telegraf/composer');
const { nonDeletingRateLimiter } = require("../utils");

const bot = new Composer();

const fetchTweet = async (screenName, postId) => {
    try {
        const response = await axios.get(`https://api.fxtwitter.com/${screenName}/status/${postId}`);
        console.log(`Fetched tweet: ${postId} from ${screenName}`);
        return response;
    } catch (error) {
        console.error(`ERROR WHILE FETCHING TWEET: ${postId} from ${screenName}`, error);
        throw error;
    }
}

const isTwitterUrl = (url) => url.startsWith("https://twitter.com/") && url.indexOf("status") > 0;

const extractUrlFromEntity = (ctx, entity) => ctx.message.text.substring(entity.offset, entity.length + entity.offset);

const parseTwitterUrl = (url) => {
    const pattern = /https:\/\/twitter.com\/(.*?)\/status\/(\d+)/;
    const match = url.match(pattern);
    if (match) {
        console.log(`Parsed URL: ${url}, ScreenName: ${match[1]}, PostId: ${match[2]}`);
        return {
            screenName: match[1],
            postId: match[2]
        };
    } else {
        console.error(`Invalid Twitter URL: ${url}`);
        throw new Error(`Invalid Twitter URL: ${url}`);
    }
}

bot.hears(
    (text, ctx) => {
        console.log("Checking for Twitter URLs...");
        if (ctx.message.entities) {
            for (const entity of ctx.message.entities.filter((x) => x.type === "text_link" || x.type === "url")) {
                const url = entity.type === "text_link" ? entity.url : extractUrlFromEntity(ctx, entity);

                if (isTwitterUrl(url)) {
                    ctx.url = url;
                    return true;
                }
            }
        }
        console.log("No Twitter URL found.");
        return false;
    },
    nonDeletingRateLimiter,
    async (ctx) => {
        try {
            const { screenName, postId } = parseTwitterUrl(ctx.url);
            const twitterResponse = await fetchTweet(screenName, postId);
            const videos = twitterResponse.data.tweet.media?.videos;

            if (videos) {
                for (const video of videos) {
                    ctx.replyWithVideo(video.url, Extra.inReplyTo(ctx.message.message_id)).catch((err) => {
                        console.log(`Error downloading video: ${video.url}`, err);
                        ctx.replyWithMarkdown(`[Ссылка](${video.url})`, Extra.inReplyTo(ctx.message.message_id));
                    });
                }
            } else {
                console.log(`No videos found in tweet: ${postId}`);
            }
        } catch (ex) {
            console.error('ERROR WHILE HANDLING TWITTER POST', ex);
        }
    }
);

module.exports = bot;
