const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
  rawMessage: {
    type: mongoose.Schema.Types.Mixed
  }
}, { timestamps: true });

const messageModel = mongoose.model("message", messageSchema);

module.exports = messageModel;
